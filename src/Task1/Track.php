<?php

declare(strict_types=1);

namespace App\Task1;

class Track
{
    private array $cars = [];
    
    public function __construct(
        private float $lapLength,
        private int $lapsNumber
    ) {
    }

    public function getLapLength(): float
    {
        return $this->lapLength;
    }

    public function getLapsNumber(): int
    {
        return $this->lapsNumber;
    }

    public function add(Car $car): void
    {
        $this->cars[] = $car;
    }

    public function all(): array
    {
        return $this->cars;
    }

    public function run(): Car
    {
        $distance = $this->lapLength * $this->lapsNumber;
        $times = [];
        
        foreach ($this->all() as $car) {
            $driveTime = $distance / $car->getSpeed();
            $stopes = (($distance / 100) / ($car->getFuelTankVolume() / $car->getFuelConsumption()));
            $times[] = $driveTime * 3600 + ($stopes * $car->getPitStopTime());
        }
        
        return $this->cars[array_keys($times, min($times))[0]];
    }
    
}